-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2019. Dec 01. 23:45
-- Kiszolgáló verziója: 10.4.8-MariaDB
-- PHP verzió: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `web2`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `blog`
--

CREATE TABLE `blog` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `shortdesc` varchar(255) DEFAULT NULL,
  `fulldesc` text DEFAULT NULL,
  `who` int(11) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `public` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `blog`
--

INSERT INTO `blog` (`id`, `title`, `shortdesc`, `fulldesc`, `who`, `postdate`, `public`) VALUES
(1, 'Teszt Blog 1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at eleifend erat. Donec fermentum ultricies purus, quis interdum mi pretium nec.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam pulvinar dolor non congue efficitur. Donec sit amet mi eu lacus consectetur blandit sit amet sed enim. Pellentesque auctor ante ex, sed porttitor ipsum rutrum sit amet. In nec hendrerit ante, facilisis elementum risus. Nunc euismod, enim vel lobortis venenatis, augue orci eleifend dolor, quis blandit urna felis sagittis felis. Sed sit amet nisi iaculis, consectetur neque et, imperdiet orci. Donec egestas ac massa sed mattis. Nam et ante nulla. In lobortis a nunc ac euismod. Suspendisse tincidunt viverra purus eget maximus. Cras non aliquam quam, id porta neque. Duis ullamcorper bibendum vestibulum.', 1, '2019-11-20 19:38:00', 1),
(2, 'Teszt Blog 2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at eleifend erat. Donec fermentum ultricies purus, quis interdum mi pretium nec.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam pulvinar dolor non congue efficitur. Donec sit amet mi eu lacus consectetur blandit sit amet sed enim. Pellentesque auctor ante ex, sed porttitor ipsum rutrum sit amet. In nec hendrerit ante, facilisis elementum risus. Nunc euismod, enim vel lobortis venenatis, augue orci eleifend dolor, quis blandit urna felis sagittis felis. Sed sit amet nisi iaculis, consectetur neque et, imperdiet orci. Donec egestas ac massa sed mattis. Nam et ante nulla. In lobortis a nunc ac euismod. Suspendisse tincidunt viverra purus eget maximus. Cras non aliquam quam, id porta neque. Duis ullamcorper bibendum vestibulum.', 1, '2019-11-21 19:43:11', 1),
(3, 'Teszt Blog 3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at eleifend erat. Donec fermentum ultricies purus, quis interdum mi pretium nec.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam pulvinar dolor non congue efficitur. Donec sit amet mi eu lacus consectetur blandit sit amet sed enim. Pellentesque auctor ante ex, sed porttitor ipsum rutrum sit amet. In nec hendrerit ante, facilisis elementum risus. Nunc euismod, enim vel lobortis venenatis, augue orci eleifend dolor, quis blandit urna felis sagittis felis. Sed sit amet nisi iaculis, consectetur neque et, imperdiet orci. Donec egestas ac massa sed mattis. Nam et ante nulla. In lobortis a nunc ac euismod. Suspendisse tincidunt viverra purus eget maximus. Cras non aliquam quam, id porta neque. Duis ullamcorper bibendum vestibulum.', 1, '2019-11-21 20:43:22', 1),
(4, 'Teszt cím', 'teszt cím', 'tesztelek...', 1, '2019-11-29 21:19:28', 1),
(5, 'Új teszt', 'újabb teszt', 'nos?????', 1, '2019-11-29 21:21:03', 1),
(6, 'Még egy teszt', 'Valamilyen leírás', 'kjfhkjsdfhgljsafélsféhflakdghdlsafjhslfjélskjadpgldfghkljdhbvmvyxbjdfégk', 1, '2019-11-29 21:22:03', 1),
(9, 'Valami', 'új valami', 'kdjfhgsklaféfgéjhasdfajshvksldjhvélkjsőlav', 1, '2019-11-29 21:26:19', 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `felhasznalok`
--

CREATE TABLE `felhasznalok` (
  `id` int(10) UNSIGNED NOT NULL,
  `csaladi_nev` varchar(45) NOT NULL DEFAULT '',
  `utonev` varchar(45) NOT NULL DEFAULT '',
  `bejelentkezes` varchar(12) NOT NULL DEFAULT '',
  `jelszo` varchar(40) NOT NULL DEFAULT '',
  `jogosultsag` varchar(3) NOT NULL DEFAULT '_1_'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `felhasznalok`
--

INSERT INTO `felhasznalok` (`id`, `csaladi_nev`, `utonev`, `bejelentkezes`, `jelszo`, `jogosultsag`) VALUES
(1, 'Rendszer', 'Admin', 'Admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', '__1'),
(2, 'Családi_2', 'Utónév_2', 'Login2', '6cf8efacae19431476020c1e2ebd2d8acca8f5c0', '_1_'),
(3, 'Családi_3', 'Utónév_3', 'Login3', 'df4d8ad070f0d1585e172a2150038df5cc6c891a', '_1_'),
(4, 'Családi_4', 'Utónév_4', 'Login4', 'b020c308c155d6bbd7eb7d27bd30c0573acbba5b', '_1_'),
(5, 'Családi_5', 'Utónév_5', 'Login5', '9ab1a4743b30b5e9c037e6a645f0cfee80fb41d4', '_1_'),
(6, 'Családi_6', 'Utónév_6', 'Login6', '7ca01f28594b1a06239b1d96fc716477d198470b', '_1_'),
(7, 'Családi_7', 'Utónév_7', 'Login7', '41ad7e5406d8f1af2deef2ade4753009976328f8', '_1_'),
(8, 'Családi_8', 'Utónév_8', 'Login8', '3a340fe3599746234ef89591e372d4dd8b590053', '_1_'),
(9, 'Családi_9', 'Utónév_9', 'Login9', 'c0298f7d314ecbc5651da5679a0a240833a88238', '_1_'),
(10, 'Családi_10', 'Utónév_10', 'Login10', 'a477427c183664b57f977661ac3167b64823f366', '_1_'),
(11, 'Béres', 'Tibor', 'adminka', '0eff96f13ae01cbbc81d944209deb618bb0cf9bb', '_1_'),
(12, 'Nagy', 'Dávid', 'davidka', '8cb2237d0679ca88db6464eac60da96345513964', '_1_'),
(15, 'Béres', 'Tibor', 'slkhdfksfdj', '68f5972a3b15dc9eed88c35ff7477fd8edc4a4c7', '_1_'),
(18, 'hozzáadott', 'rendszerrel', '012', 'teszt', '_1_');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `menu`
--

CREATE TABLE `menu` (
  `url` varchar(30) NOT NULL,
  `nev` varchar(30) NOT NULL,
  `szulo` varchar(30) NOT NULL,
  `jogosultsag` varchar(3) NOT NULL,
  `sorrend` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `menu`
--

INSERT INTO `menu` (`url`, `nev`, `szulo`, `jogosultsag`, `sorrend`) VALUES
('admin', 'Admin', '', '001', 40),
('belepes', 'Belépés', '', '100', 50),
('blog', 'Blog', '', '011', 30),
('kilepes', 'Kilépés', '', '011', 60),
('nyitolap', 'Nyitólap', '', '111', 10),
('regisztracio', 'Regisztráció', '', '100', 20);

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `felhasznalok`
--
ALTER TABLE `felhasznalok`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`url`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT a táblához `felhasznalok`
--
ALTER TABLE `felhasznalok`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
