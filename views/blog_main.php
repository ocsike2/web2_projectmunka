<?php if ( count( $viewData['uzenet'] > 0 ) ) {
    $i = 0;
    while($i < count($viewData['uzenet'])) {
        echo "<h2>";
        ?>
        <a href="<?php echo SITE_ROOT ?>blog/bovebben"><?php echo $viewData['uzenet'][$i]['title']; ?></a></h2>
        <?php
        echo "<h4>Írta: ".$viewData['uzenet'][$i]['who'].", Dátum: ".$viewData['uzenet'][$i]['postdate']."</h4>";
        echo "<p>".$viewData['uzenet'][$i]['shortdesc']."</p>";
        $i++;
    }
}
?>

<section style="display: none" id="newpost">
    <h2>Új poszt hozzáadása</h2>
    <form action="<?php echo SITE_ROOT ?>addnewpost" method="post">
        <label for="login">Poszt címe:</label><input type="text" name="title"><br>
        <label for="login">Rövid leírása:</label><input type="text" name="shortdesc"><br>
        <label for="login">Poszt szövege:</label>&nbsp;<textarea name="fulldesc" rows="10" cols="50"></textarea><br>
        <input type="submit" value="Küldés" name="sendnewpost">
    </form>
    <h2><br><br></h2>
</section>

<input type="button" id="addpost" value="Add a new post">

<script type="text/javascript">
    $(document).ready(function(){
        $( "#addpost" ).on( "click", function() {
            $("#newpost").show("slow");
        });
    });
</script>
