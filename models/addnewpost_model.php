<?php

class Addnewpost_Model
{
	public function get_data($vars)
	{
		$retData['eredmeny'] = "";
		try {
			$connection = Database::getConnection();
            $sql = "INSERT INTO blog (title, shortdesc, fulldesc, who, postdate, public) VALUES (?, ?, ?, ?, ?, ?)";
            $stmt = $connection->prepare($sql);
            $stmt->execute([$vars['title'], $vars['shortdesc'], $vars['fulldesc'], $_SESSION['userid'], date("Y-m-d H:i:s"), 1]);
		}
		catch (PDOException $e) {
					$retData['eredmeny'] = "ERROR";
					$retData['uzenet'] = "Adatbázis hiba: ".$e->getMessage()."!";
		}
		return $retData;
	}
}

?>