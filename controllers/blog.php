<?php

class Blog_Controller
{
    public $baseName = 'blog';  //meghatározni, hogy melyik oldalon vagyunk
    public function main(array $vars) // a router által továbbított paramétereket kapja
    {
        $blogModel = new Blog_Model;  //az osztályhoz tartozó modell
        //a modellben belépteti a felhasználót
        $retData = $blogModel->get_data($vars);

        //betöltjük a nézetet
        $view = new View_Loader($this->baseName."_main");
        foreach($retData as $name => $value)
            $view->assign($name, $value);

    }
}
