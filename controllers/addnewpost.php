<?php

class Addnewpost_Controller
{
	public $baseName = 'blog';  //meghatározni, hogy melyik oldalon vagyunk
	public function main(array $vars) // a router által továbbított paramétereket kapja
	{
	    if (!empty($vars['title']) && !empty($vars['shortdesc']) && !empty($vars['fulldesc'])) {
            $addnewpostModel = new Addnewpost_Model;  //az osztályhoz tartozó modell
            //a modellben belépteti a felhasználót
            $retData = $addnewpostModel->get_data($vars);
        }
        header("Location: ".SITE_ROOT.$this->baseName);
	}
}

?>